'use strict';
const execBuffer = require('exec-buffer');
const isJpg = require('is-jpg');
const jpegtran = require('jpegtran-bin');
const tmp = require('tmp-promise');
const fs = require('fs').promises;

module.exports = opts => buf => {
	opts = Object.assign({}, opts);

	if (!Buffer.isBuffer(buf)) {
		return Promise.reject(new TypeError('Expected a buffer'));
	}

	if (!isJpg(buf)) {
		return Promise.resolve(buf);
	}

	const args = ['-copy', 'none'];

	if (opts.progressive) {
		args.push('-progressive');
	}

	if (opts.arithmetic) {
		args.push('-arithmetic');
	} else {
		args.push('-optimize');
	}

	let processScans = Promise.resolve();

	if (opts.scans && opts.scans.length) {
		let scans = opts.scans.map((scan) => {
			return scan.map((pass) => {
				let components = [];
				components[0] = (Array.isArray(pass[0]) ? pass[0].join(',') : pass[0]) + ':';
				components[1] = `${pass[1]}-${pass[2]},`;
				components[2] = `${pass[3]},`;
				components[3] = `${pass[4]};`;
				return components.join(' ');
			}).join("\n");
		});

		const scanFiles = [];
		const promises = scans.map((scan) => {
			return tmp.file()
				.then((tmpFile) => {
					scanFiles.push(tmpFile.path);
					return fs.writeFile(tmpFile.path, scan);
				})
		});

		processScans = Promise.all(promises)
			.then(() => {
				args.push('-scans');
				args.push(scanFiles.join(' '));
				return true;
			});
	}

	return processScans.then(() => {
		args.push('-outfile', execBuffer.output, execBuffer.input);

		return execBuffer({
			input: buf,
			bin: jpegtran,
			args
		}).catch(error => {
			error.message = error.stderr || error.message;
			throw error;
		});
	});
};
