# imagemin-jpegtran [![Build Status](https://travis-ci.org/imagemin/imagemin-jpegtran.svg?branch=master)](https://travis-ci.org/imagemin/imagemin-jpegtran)

> jpegtran imagemin plugin


## Install

```
$ npm install --save imagemin-jpegtran
```


## Usage

```js
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');

imagemin(['images/*.jpg'], 'build/images', {use: [imageminJpegtran()]}).then(() => {
	console.log('Images optimized');
});
```


## API

### imageminJpegtran([options])(buffer)

Returns a promise for a buffer.

#### options

##### progressive

Type: `boolean`<br>
Default: `false`

Lossless conversion to progressive.

##### arithmetic

Type: `boolean`<br>
Default: `false`

Use [arithmetic coding](http://en.wikipedia.org/wiki/Arithmetic_coding).

##### scans

Type: `Array`<br>
Default: `[]`

Specify custom scans.

e.g.
```
[
    [
        [[0, 1, 2], 0, 0, 0, 1],
        [0, 1, 27, 0, 0],
        [2, 1, 63, 0, 0],
        [1, 1, 63, 0, 0],
        [0, 28, 63, 0, 0],
    ],
]
```

#### buffer

Type: `buffer`

Buffer to optimize.


## License

MIT © [imagemin](https://github.com/imagemin)
